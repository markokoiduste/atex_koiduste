package custommock;

import common.BankService;
import common.Money;

import java.util.Objects;

public class TestableBankService implements BankService {
    private Money money;
    private Money toMoney;
    private Money fromMoney;
    private String fromAccount;
    private String toAccount;
    private boolean hasFunds = true;
    private boolean withdrawCalled = false;

    public boolean wasWithdrawCalledWith(Money money, String account) {
        // siin peaks võrdlema praeguseid argumente meeldejäetud argumentidega
        //return (Objects.equals(account, fromAccount) && fromMoney == money);
        if (money == null) return false;
        if (account.equals(null)) return false;
        return (convert(money, this.money.getCurrency()).getAmount() == this.money.getAmount()) && account.equals(this.fromAccount);
    }

    public boolean wasDepositCalledWith(Money money, String account) {
        // siin peaks võrdlema praeguseid argumente meeldejäetud argumentidega
        //return (Objects.equals(account, toMoney) && money == toMoney);
        //return (money.equals(this.toMoney) && account.equals(this.toAccount));
        if (money == null) return false;
        if (account.equals(null)) return false;
        return (convert(money, this.money.getCurrency()).getAmount() == this.money.getAmount()) && account.equals(this.toAccount);
    }

    @Override
    public void withdraw(Money money, String fromAccount) {
        System.out.println("credit: " + money + " - " + fromAccount);
        //this.money = money;
        this.fromMoney = money;
        this.money = money;
        this.fromAccount = fromAccount;
        this.withdrawCalled = true;
        // siin peaks argumendid (money ja fromAccount) meelde jätma

    }

    @Override
    public void deposit(Money money, String toAccount) {
        System.out.println("debit: " + money + " - " + toAccount);
        //this.money = money;
        this.toMoney = money;
        this.money = money;
        this.toAccount = toAccount;
        // siin peaks argumendid (money ja toAccount) meelde jätma

    }

    @Override
    public Money convert(Money money, String targetCurrency) {
        if (money.getCurrency().equals(targetCurrency)) return money;

        double rate = 1.0/10;

        return new Money((int) (money.getAmount() / rate), targetCurrency);
    }

    @Override
    public String getAccountCurrency(String account) {
        switch (account) {
            case "E_123": return "EUR";
            case "S_456": return "SEK";
            default: throw new IllegalStateException();
        }
    }

    @Override
    public boolean hasSufficientFundsFor(Money requiredAmount, String account) {
        return hasFunds;
    }

    public void setSufficentFundsAvailable(boolean areFundsAvailable) {
        hasFunds = areFundsAvailable;
        // hasSufficientFundsFor() tagastab praegu alati true.
        // See meetod peaks määrama, kuidas hasSufficientFundsFor() vastab.
    }

    public boolean wasWithdrawCalled() {
        return this.withdrawCalled;
        // Meetod peaks ütema, kas meetodit credit() välja kutsuti
        // (mistahes argumentidega)
    }

}