package rpn;

import java.util.Stack;

/**
 * Created by Marko on 17-Sep-16.
 */
public class RpnCalculator implements IRpnCalculator {

    private Stack data;
    private int accumulator;

    public RpnCalculator() {
        this.data = new Stack();
        this.accumulator = 0;
    }

    @Override
    public int getAccumulator() {
        return this.accumulator;
    }

    @Override
    public void setAccumulator(int x) {
        this.accumulator = x;
    }

    @Override
    public void enter() {
        this.data.push(getAccumulator());
        setAccumulator(0);
    }

    @Override
    public void plus() {
        setAccumulator(getAccumulator()+(int)data.pop());
    }

    @Override
    public void multiply() {
        setAccumulator(getAccumulator()*(int)data.pop());
    }

    @Override
    public int evaluate(String expr) {
        return IntStack.interpret(expr); //Ei hakanud ratast uuesti leiutama, kasutasin kunagi ise tehtud lahendust
    }
}
