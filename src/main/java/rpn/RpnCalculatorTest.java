package rpn;


import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * Created by Marko on 17-Sep-16.
 */
public class RpnCalculatorTest {

    @Test
    public void initialCalculatorHasAccumulatorZero()
    {
        IRpnCalculator c = new RpnCalculator();

        assertThat(c.getAccumulator(), is(0));
    }

    @Test
    public void canSetAccumulatorManually() {
        IRpnCalculator c = new RpnCalculator();
        int a = 6;

        c.setAccumulator(a);
        assertThat(c.getAccumulator(), is(a));
    }

    @Test
    public void canAddTwoNumbers() {
        IRpnCalculator c = new RpnCalculator();
        int a = 1, b = 2;
        c.setAccumulator(a);
        c.enter();
        c.setAccumulator(b);
        c.plus();
        assertThat(c.getAccumulator(), is(a+b));
    }

    @Test
    public void canAddAndMultiply() {
        IRpnCalculator c = new RpnCalculator();
        int a = 1, b = 2, x = 4;

        c.setAccumulator(a);
        c.enter();
        c.setAccumulator(b);
        c.plus();
        c.enter();
        c.setAccumulator(x);
        c.multiply();
        assertThat(c.getAccumulator(), is((a+b)*x));
    }

    @Test
    public void canAddAndMultiplyMoreComplex(){
        IRpnCalculator c = new RpnCalculator();

        c.setAccumulator(4);
        c.enter();
        c.setAccumulator(3);
        c.plus();
        c.enter();
        c.setAccumulator(2);
        c.enter();
        c.setAccumulator(1);
        c.plus();
        c.multiply();
        assertThat(c.getAccumulator(), is(21));
    }

    @Test
    public void canEvaluate() {
        IRpnCalculator c = new RpnCalculator();

        assertThat(c.evaluate("5 1 2 + 4 * + 3 +"), is(20));
    }
}
