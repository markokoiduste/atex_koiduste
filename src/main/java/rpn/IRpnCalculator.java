package rpn;

/**
 * Created by Marko on 17-Sep-16.
 */
public interface IRpnCalculator {
    int getAccumulator();
    void setAccumulator(int x);
    void enter();
    void plus();
    void multiply();
    int evaluate(String expr);
}
