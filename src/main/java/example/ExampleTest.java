package example;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class ExampleTest {

    @Test
    public void testName() throws Exception {
        int result = 0;

        assertThat(result, is(0));

        assertThat("x", sameInstance("x"));

        assertThat("x", not("y"));

        assertThat(null, nullValue());

        assertThat("", notNullValue());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNameThrows() throws Exception {
        throw new IllegalArgumentException();
    }

}
