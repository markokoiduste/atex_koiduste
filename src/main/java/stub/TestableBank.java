package stub;

import common.Money;

public class TestableBank extends Bank {

    @Override
    public Money convert(Money money, String toCurrency) {

        if (money.getCurrency().equals(toCurrency)) {
            return money;
        } else {
            //Best way would be to make a table of currency rates,
            //this would avoid building an endless switch.
            // Currency name would be mapped to a currency rate.
            if (toCurrency.equals("EUR") && money.getCurrency().equals("EEK")) {
                return new Money(money.getAmount() / 15, toCurrency);
            } else if (toCurrency.equals("EEK") && money.getCurrency().equals("EUR")) {
                return new Money(money.getAmount() * 15, toCurrency);
            }
        }

        throw new RuntimeException("No conversion between selected currencies described.");
    }
}
