package gol;

public class Frame {
    private int x, y;
    private String[][] frame;

    public Frame(int width, int height) {
        x = width;
        y = height;
        this.frame = new String[x][y];
        initFrame();
    }

    public Frame(String[] frameData) {
        x = frameData.length;
        y = frameData[0].length();
        this.frame = new String[x][y];
        createFrame(frameData);
    }

    private void initFrame() {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                markDead(i, j);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                sb.append(isAlive(i, j) ? 'o' : '-');
            }
            sb.append(System.getProperty("line.separator"));
        }
        return sb.toString();
    }


    @Override
    public boolean equals(Object obj) {
        return getClass() == obj.getClass() && (this.toString().equals(obj.toString()));
    }

    public Integer getNeighbourCount(int x, int y) {
        int count = 0;
        for(int i = x - 1; i <= x + 1; i++){
            if (i >= 0 && i < this.x) {
                for (int j = y - 1; j <= y + 1; j++) {
                    if (j >= 0 && j < this.y) {
                        if ((i != x || j != y) && isAlive(i, j)) {
                            count++;
                        }
                    }
                }
            }
        }
        return count;
    }

    public boolean isAlive(int i, int j) {
        return frame[i][j].matches("o");
    }

    public void markAlive(int i, int j) {
        this.frame[i][j] = "o";
    }

    public void markDead(int i, int j) {
        this.frame[i][j] = "-";
    }

    public void createFrame(String[] frameData) {
        for (int i = 0; i < x; i++) {
            String[] row = frameData[i].split("");
            for (int j = 0; j < y; j++) {
                if (row[j].equalsIgnoreCase("-")) {
                    markDead(i, j);
                } else {
                    markAlive(i, j);
                }
            }
        }
    }

    Frame nextFrame() {
        Frame nFrame = this.copyFrame();
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                int nCount = getNeighbourCount(i, j);
                if (nCount == 2) continue;
                if (nCount == 3) nFrame.markAlive(i, j);
                else nFrame.markDead(i, j);
            }
        }
        return nFrame;
    }

    String[] getFrameData(Frame frame) {
        return frame.toString().split(System.getProperty("line.separator"));
    }

    Frame copyFrame() {
        return new Frame(getFrameData(this));
    }
}