package gol;


import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

@SuppressWarnings("unused")
public class GameOfLifeTest {

    // Võiks alustada sellest, et kaadris on võimalik rakke elusaks märkida.

    @Test
    public void markCellAlive() {
        Frame frame = new Frame (10, 10);
        frame.markAlive(5, 5);
        assertThat(frame.isAlive(5, 5), is(true));
    }

    // Siis võiks proovida naabreid lugeda.

    @Test
    public void countNeighbours() {
        Frame frame = getFrame(
                "-oo-",
                "--o-",
                "o---",
                "o--o");
        assertThat(frame.getNeighbourCount(0, 2), is(2));
        assertThat(frame.getNeighbourCount(2, 0), is(1));
        assertThat(frame.getNeighbourCount(3, 3), is(0));
    }

    // Siis võiks järgmist kaadrit arvutada.
    @Test
    public void gliderWorks() {
        Frame frame1 = getFrame(
                "-o----",
                "--oo--",
                "-oo---",
                "------");

        Frame frame2 = getFrame(
                "--o---",
                "---o--",
                "-ooo--",
                "------");

        Frame frame3 = getFrame(
                "------",
                "-o-o--",
                "--oo--",
                "--o---");

        assertThat(frame1.nextFrame(), is(equalTo(frame2)));
        assertThat(frame2.nextFrame(), is(equalTo(frame3)));
    }

    @Test
    public void pulsarWorks() {
        Frame frame1 = getFrame(
                "------",
                "-oo---",
                "-o----",
                "----o-",
                "---oo-",
                "------");

        Frame frame2 = getFrame(
                "------",
                "-oo---",
                "-oo---",
                "---oo-",
                "---oo-",
                "------");

        assertThat(frame1.nextFrame(), is(equalTo(frame2)));
        assertThat(frame1.nextFrame().nextFrame(), is(equalTo(frame1)));
    }

    @Test
    public void extinctWorks() {
        Frame frame1 = getFrame(
                "----",
                "-o--",
                "--o-",
                "----");
        Frame frame2 = getFrame(
                "----",
                "----",
                "----",
                "----");
        assertThat(frame1.nextFrame(), is(equalTo(frame2)));
    }

    @Test
    public void stabilizedWorks() {
        Frame frame1 = getFrame("------",
                "--oo--",
                "-o--o-",
                "--oo--",
                "------");
        assertThat(frame1.nextFrame(), is(equalTo(frame1)));
    }

    // Esialgu mõnda väga lihtsat.

    // Kui vajate abimeetodeid Nt. Frame.toString() siis kirjutage testid ka neile.

    // Soovitatav on kasutada Tdd praktikat.

    private Frame getFrame(String... frameData) {
        return new Frame(frameData);
    }
}