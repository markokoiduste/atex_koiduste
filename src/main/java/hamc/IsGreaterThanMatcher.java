package hamc;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
 * Created by Marko on 12-Jan-17.
 */
public class IsGreaterThanMatcher extends TypeSafeMatcher<Integer>{

    private Integer expected;

    public IsGreaterThanMatcher(Integer expected) {
        this.expected = expected;
    }

    @Override
    protected boolean matchesSafely(Integer integer) {
        return integer > expected;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("is greater than " + expected);
    }
}
