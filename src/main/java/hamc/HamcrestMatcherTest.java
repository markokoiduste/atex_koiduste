package hamc;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.hamcrest.core.Is;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

/**
 * Created by Marko on 12-Jan-17.
 */
public class HamcrestMatcherTest {

    private IsGreaterThanMatcher isGreaterThan(int integer) {
        return new IsGreaterThanMatcher(integer);
    }

    @Test
    public void testGreaterNumberToSmaller() {
        assertThat(2, is(isGreaterThan(1)));
    }

    @Test
    public void testSmallerNumberToGreater() {
        assertThat(1, is(not(isGreaterThan(2))));
    }

    @Test
    public void testEqualNumbers() {
        assertThat(1, is(not(isGreaterThan(1))));
    }


}
