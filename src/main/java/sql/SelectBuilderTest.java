package sql;

import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SelectBuilderTest {

    @Test
    public void selectFrom() {
        SelectBuilder b = new SelectBuilder();
        b.column("a");
        b.from("t");

        assertThat(b.getSql(), is("select a from t"));
    }

    @Test
    public void selectTwoFrom() {
        SelectBuilder b = new SelectBuilder();
        b.columns("a", "b");
        b.from("t");

        assertThat(b.getSql(), is("select a, b from t"));
    }

    @Test
    public void selectFromById() {
        SelectBuilder b = new SelectBuilder();
        b.column("a");
        b.from("t");
        b.where("id = ?", 1);

        assertThat(b.getSql(), is("select a from t where id = ?"));
        assertThat(b.getParameters(), is(Arrays.asList(1)));

    }

    @Test
    public void selectFromByComplexCondition() {
        SelectBuilder b = new SelectBuilder();
        b.column("a");
        b.from("t");
        b.where("is_hidden = 1");
        b.where("deleted_on is null");

        assertThat(b.getSql(),
                is("select a from t where is_hidden = 1 and deleted_on is null"));
    }

    @Test
    public void selectFromWhereConditionsNotNull() {
        SelectBuilder b = new SelectBuilder();
        b.column("a");
        b.from("t");
        b.eqIfNotNull("a", 1);
        b.eqIfNotNull("b", null);
        b.eqIfNotNull("c", 3);

        assertThat(b.getSql(), is("select a from t where a = ? and c = ?"));
        assertThat(b.getParameters(), is(Arrays.asList(1, 3)));
    }

    @Test
    public void selectIn() {
        SelectBuilder b = new SelectBuilder();
        b.column("a");
        b.from("t");
        b.in("id", Arrays.asList(1, 2));

        assertThat(b.getSql(), is("select a from t where id in (?, ?)"));
        assertThat(b.getParameters(), is(Arrays.asList(1, 2)));
    }

    @Test
    public void selectLeftJoin() {
        SelectBuilder b = new SelectBuilder();
        b.column("a");
        b.from("t");
        b.leftJoin("u", "u.id = t.u_id");

        assertThat(b.getSql(), is("select a from t left join u on u.id = t.u_id"));
    }

    @Test
    public void selectFromOtherSelect() {
        SelectBuilder sub = new SelectBuilder();
        sub.column("b");
        sub.from("t");

        SelectBuilder b = new SelectBuilder();
        b.column("a");
        b.from(sub);

        assertThat(b.getSql(), is("select a from (select b from t)"));
    }
}

