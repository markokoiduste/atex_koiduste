package sql;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SelectBuilder {
    private List<String> columns = new ArrayList<String>();
    private String from = "";
    private List<String> where = new ArrayList<String>();
    private List<Object> parameters = new ArrayList<Object>();
    private String leftJoin ="";

    public SelectBuilder(){

    }

    public void column(String column) {
        this.columns.add(column);
    }

    public void from(String table) {
        from = table;
    }

    public String getSql() {
        if(leftJoin.length()>1){
            return MessageFormat.format("select {0} from {1} {2}",
                    String.join(", ", columns),
                    from,
                    leftJoin);
        }
        if (where.size()==0) {
            return MessageFormat.format("select {0} from {1}",
                    String.join(", ", columns),
                    from);
        }
        return MessageFormat.format("select {0} from {1} where {2}",
                String.join(", ", columns),
                from,
                String.join(" and ", where));
    }



    public void columns(String ... columns) {
        this.columns.addAll(Arrays.asList(columns));
    }

    public void where(String condition, Object parameter) {
        where.add(condition);
        parameters.add(parameter);
    }

    public void where(String condition) {
        where.add(condition);
    }

    public List<Object> getParameters() {
        return parameters;
    }

    public void eqIfNotNull(String column, Object parameter) {
        if(parameter!=null){
            where.add(MessageFormat.format("{0} = ?",column));
            parameters.add(parameter);
        }
    }

    public void leftJoin(String table, String condition) {
        leftJoin = MessageFormat.format("left join {0} on {1}",
                table, condition);

    }

    public void in(String column, List<Object> pmers) {
        if(!pmers.isEmpty()){
            String param ="?";
            for (int i =0; i<pmers.size()-1;i++) {
                param+=", ?";
            }

            where.add(MessageFormat.format("{0} in ({1})",
                    column,
                    param
            ));
            parameters = pmers;
        }
    }

    public void from(SelectBuilder sub) {
        from = MessageFormat.format("{0}({1})",
                from,
                sub.getSql());

    }

}