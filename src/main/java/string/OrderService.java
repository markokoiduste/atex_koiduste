package string;

import java.util.*;

@SuppressWarnings("unused")
public class OrderService {

    private DataSource dataSource;

    public OrderService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Order> getFilledOrders() {
        List<Order> orders = new ArrayList<>();
        for(Order o : dataSource.getOrders()){
            if(o.isFilled()) {
                orders.add(o);
            }
        }
        return orders;
    }

    public List<Order> getOrdersOver(double amount) {
        List<Order> orders = new ArrayList<>();
        for(Order o : dataSource.getOrders()){
            if(o.getTotal() > amount) {
                orders.add(o);
            }
        }
        return orders;
    }

    public List<Order> getOrdersSortedByDate() {
        List<Order> orders = dataSource.getOrders();
        Collections.sort(orders, new DateComparator());
        return orders;
    }
}

class DateComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        Date d1 = ((Order) o1).getOrderDate();
        Date d2 = ((Order) o2).getOrderDate();
        return d1.compareTo(d2);
    }
}
