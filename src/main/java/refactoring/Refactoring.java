package refactoring;

import java.util.*;

public class Refactoring {

    public static final double VAT = 1.2;
    public static final double OVERTIME_RATE = 1.5;

    // 1a
    public int incrementInvoiceNumber() {
        return ++invoiceNumber;
    }

    // 1b
    public void fillListWithFilledOrders(List<Order> orderList) {
        for (Order order : orders) {
            if (order.isFilled()) {
                orderList.add(order);
            }
        }
    }

    // 2a
    public void printStatementFor(Long invoiceId) {
        List<InvoiceRow> invoiceRows = dao.getInvoiceRowsFor(invoiceId);
        printInvoiceRows(invoiceRows);
        printValue(invoiceTotal(invoiceRows));
    }

    private double invoiceTotal(List<InvoiceRow> invoiceRows) {
        double total = 0;
        for (InvoiceRow invoiceRow : invoiceRows) {
            total += invoiceRow.getAmount();
        }
        return total;
    }

    // 2b
    public String getItemsAsHtml() {
        return buildHtmlList(items);
    }

    public static String buildHtmlList(List<String> items) {
        String retval = "";
        retval += "<ul>";
        for(String item: items) {
            retval += "<li>" + item + "</li>";
        }
        retval += "</ul>";
        return retval;
    }

    // 3
    public boolean isSmallOrder() {
        return  order.getTotal() < 100;
    }

    // 4
    public void printPrice() {
        System.out.println("Hind ilma käibemaksuta: " + getBasePrice());
        System.out.println("Hind käibemaksuga: " + getBasePrice() * VAT);
    }

    // 5
    public void calculatePayFor(Job job) {
        // on holiday at night
        boolean isHoliday = (job.day == 6 || job.day == 7);
        boolean isNightTime = (job.hour > 20 || job.hour < 7);
        if (isHoliday && isNightTime)  {

        }
    }

    // 6
    public boolean canAccessResource(SessionData sessionData) { return isAdmin(sessionData) && hasPreferredStatus(sessionData); }

    public boolean hasPreferredStatus(SessionData sessionData) {
        return sessionData.getStatus().equals("preferredStatusX")
            || sessionData.getStatus().equals("preferredStatusY");
    }

    public boolean isAdmin(SessionData sessionData) {
        return sessionData.getCurrentUserName().equals("Admin")
            || sessionData.getCurrentUserName().equals("Administrator");
    }

    // 7
    public void drawLines() {
        Space space = new Space();
        space.drawLine(new Point(12, 3, 5), new Point(2, 4, 6));
        space.drawLine(new Point(2, 4, 6), new Point(0, 1, 0));
    }

    // 8
  /*  public int calculateWeeklyPay(int hoursWorked, boolean overtime) {
        int straightTime = Math.min(40, hoursWorked);
        int overTime = Math.max(0, hoursWorked - straightTime);
        int straightPay = straightTime * hourRate;
        double overtimeRate = overtime ? 1.5 * hourRate : 1.0 * hourRate;
        int overtimePay = (int) Math.round(overTime * overtimeRate);
        return straightPay + overtimePay;
    } */

    public int calculateWeeklyPayWithOverTime(int hours) {
        return Math.min(40, hours) * hourRate + (int)Math.round(OVERTIME_RATE * hourRate * Math.max(0, hours - (Math.min(40, hours))));
    }

    public int calculateWeeklyPayWithoutOverTime(int hours) {
        return Math.round(hourRate * hours);
    }

    // //////////////////////////////////////////////////////////////////////////

    // Abiväljad ja abimeetodid.
    // Need on siin lihtsalt selleks, et kood kompileeruks

    private List<String> items = Arrays.asList("1", "2", "3", "4");
    private int hourRate = 5;
    int invoiceNumber = 0;
    private List<Order> orders = new ArrayList<Order>();
    private Order order = new Order();
    private Dao dao = new SampleDao();
    private double price = 0;

    void justACaller() {
        incrementInvoiceNumber();
        fillListWithFilledOrders(null);
    }

    private void printValue(double total) {
    }

    private void printInvoiceRows(List<InvoiceRow> invoiceRows) {
    }

    class Point {
        private final int x;
        private final int y;
        private final int z;

        private Point(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public int getZ() {
            return z;
        }
    }

    class Space {
        public void drawLine(Point start, Point end) {
        }
    }

    interface Dao {
        List<InvoiceRow> getInvoiceRowsFor(Long invoiceId);
    }

    class SampleDao implements Dao {
        @Override
        public List<InvoiceRow> getInvoiceRowsFor(Long invoiceId) {
            return null;
        }
    }

    class Order {
        public boolean isFilled() {
            return false;
        }

        public double getTotal() {
            return 0;
        }
    }

    class InvoiceRow {
        public double getAmount() {
            return 0;
        }
    }

    class Job {
        public int hour;
        public int day;
    }

    private double getBasePrice() {
        return price;
    }

    private class SessionData {

        public String getCurrentUserName() {
            return null;
        }

        public String getStatus() {
            return null;
        }

    }

}
