package refactoring;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class RefactoringTests {

    private Refactoring sut = new Refactoring();

    @Test
    public void getsItemsAsHtml() throws Exception {
        assertThat(sut.getItemsAsHtml(), is(
                "<ul><li>1</li><li>2</li><li>3</li><li>4</li></ul>"));
    }

    @Test
    public void calculatesWeeklyPayWithOvertime() {
        assertThat(sut.calculateWeeklyPayWithOverTime(39), is(195));
        assertThat(sut.calculateWeeklyPayWithOverTime(41), is(208));
    }

    @Test
    public void calculatesWeeklyPayWithoutOvertime() {
        assertThat(sut.calculateWeeklyPayWithoutOverTime(39), is(195));
        assertThat(sut.calculateWeeklyPayWithoutOverTime(41), is(205));
    }
}
