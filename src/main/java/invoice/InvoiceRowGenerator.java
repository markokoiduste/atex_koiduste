package invoice;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class InvoiceRowGenerator {

    @SuppressWarnings("unused")
    private InvoiceRowDao invoiceRowDao;

    public void generateRowsFor(BigDecimal amount, Date start, Date end) {

        List<Date> dates = generateDates(start, end);
        List<BigDecimal> payments = generatePayments(amount, dates.size());

        while(!payments.isEmpty()) {
            invoiceRowDao.save(new InvoiceRow(payments.remove(0), dates.remove(0)));
        }
    }

    private List<Date> generateDates(Date start, Date end) {
        List<Date> dates = new ArrayList<>();
        dates.add(start);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(start);

        Calendar endCalendar = Calendar.getInstance();
        endCalendar.setTime(end);
        endCalendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        end = endCalendar.getTime();

        while(calendar.getTime().before(end)) {
            calendar.add(Calendar.MONTH, 1);
            calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            dates.add(calendar.getTime());
        }

        return dates;
    }

    private List<BigDecimal> generatePayments(BigDecimal amount, int months) {
        List<BigDecimal> payments = new ArrayList<>();

        if (amount.intValueExact() < 3) {
            payments.add(amount);
            return payments;
        }

        if (amount.intValueExact() / months < 3) {
            payments.add(amount.subtract(new BigDecimal(3.)));
            payments.add(new BigDecimal(3.));
            return payments;
        }

        while (months > 0) {
            BigDecimal payment = amount.divide(new BigDecimal(months), RoundingMode.UP);
            boolean isFirst = amount.divide(new BigDecimal(3), RoundingMode.HALF_UP).doubleValue() < 1;

            if (isFirst) {
                payments.add(0, new BigDecimal(3));
                return payments;
            }

            payments.add(0, payment);
            amount = amount.subtract(payment);
            months--;
        }

        return payments;
    }
}
