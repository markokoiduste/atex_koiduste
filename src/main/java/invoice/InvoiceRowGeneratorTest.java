package invoice;

import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.text.*;
import java.util.Date;

import org.hamcrest.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class InvoiceRowGeneratorTest {

    @Mock
    InvoiceRowDao dao;

    @InjectMocks
    InvoiceRowGenerator generator;

    @Test
    public void dividesEquallyWhenNoRemainder() {

        generator.generateRowsFor(new BigDecimal(9), asDate("2012-02-15"), asDate("2012-04-02"));

        verify(dao, times(3)).save(argThat(getMatcherForAmount(3)));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2012-02-15"))));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2012-03-01"))));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2012-04-01"))));
        Mockito.verifyNoMoreInteractions(dao);
        // verify that there are no more calls
    }

    @Test
    public void generatesPaymentDates() {
        generator.generateRowsFor(new BigDecimal(9), asDate("2017-01-03"), asDate("2017-03-02"));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2017-01-03"))));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2017-02-01"))));
        verify(dao, times(1)).save(argThat(getMatcherForDate(asDate("2017-03-01"))));
        Mockito.verifyNoMoreInteractions(dao);
    }

    @Test
    public void generatesPaymentAmountsEqually() {
        generator.generateRowsFor(new BigDecimal(9), asDate("2017-01-03"), asDate("2017-03-02"));
        verify(dao, times(3)).save(argThat(getMatcherForAmount(3)));
        Mockito.verifyNoMoreInteractions(dao);
    }

    @Test
    public void generatesPaymentAmountsInequally() {
        generator.generateRowsFor(new BigDecimal(10), asDate("2017-01-03"), asDate("2017-03-02"));
        verify(dao, times(2)).save(argThat(getMatcherForAmount(3)));
        verify(dao, times(1)).save(argThat(getMatcherForAmount(4)));
        Mockito.verifyNoMoreInteractions(dao);
    }

    @Test
    public void concatsSmallPaymentAmounts() {
        generator.generateRowsFor(new BigDecimal(7), asDate("2017-01-03"), asDate("2017-03-02"));
        verify(dao, times(1)).save(argThat(getMatcherForAmount(3)));
        verify(dao, times(1)).save(argThat(getMatcherForAmount(4)));
        Mockito.verifyNoMoreInteractions(dao);
    }

    @Test
    public void generatesSingleSmallPayment() {
        generator.generateRowsFor(new BigDecimal(2), asDate("2017-01-03"), asDate("2017-03-02"));
        verify(dao, times(1)).save(argThat(getMatcherForAmount(2)));
        Mockito.verifyNoMoreInteractions(dao);
    }



    private Matcher<InvoiceRow> getMatcherForAmount(final Integer amount) {

        // Matcher-i näide. Sama põhimõttega tuleb teha teine
        // Kuupäeva jaoks

        return new TypeSafeMatcher<InvoiceRow>() {

            @Override
            protected boolean matchesSafely(InvoiceRow item) {
                return amount.equals(item.amount.intValue());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(String.valueOf(amount));
            }
        };
    }

    private Matcher<InvoiceRow> getMatcherForDate(final Date date) {
        return new TypeSafeMatcher<InvoiceRow>() {

            @Override
            public void describeTo(Description description) {
                description.appendValue(date);
            }

            @Override
            protected boolean matchesSafely(InvoiceRow invoiceRow) {
                return date.equals(invoiceRow.date);
            }
        };
    }

    public static Date asDate(String date) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

}