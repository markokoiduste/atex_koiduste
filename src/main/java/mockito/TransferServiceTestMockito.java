package mockito;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.mockito.Mockito;

import common.BankService;
import common.TransferService;
import common.Money;

@SuppressWarnings("unused")
public class TransferServiceTestMockito {

    BankService bankService = mock(BankService.class);
    TransferService transferService = new TransferService(bankService);

    @Test
    public void transferWithCurrencyConversion() {

        when(bankService.getAccountCurrency("E_123")).thenReturn("EUR");
        // when(...
        // seda mida on vaja õpetada leiab transferService.transfer() meetodist.

        // kanda 1 EUR kontolt E_123 kontole S_456
        // E_123 konto valuuta on EUR
        // S_456 konto valuuta on SEK
        when(bankService.convert(new Money(1, "EUR"), "EUR")).thenReturn(new Money(1, "EUR"));
        when(bankService.hasSufficientFundsFor(new Money(1, "EUR"), "E_123")).thenReturn(true);
        when(bankService.getAccountCurrency("S_456")).thenReturn("SEK");
        when(bankService.convert(new Money(1, "EUR"), "SEK")).thenReturn(new Money(10, "SEK"));
        transferService.transfer(new Money(1, "EUR"), "E_123", "S_456");

        verify(bankService).withdraw(new Money(1, "EUR"), "E_123");
        verify(bankService).deposit(new Money(10, "SEK"), "S_456");
    }

    @Test
    public void transferWhenNotEnoughFunds() {
        when(bankService.getAccountCurrency("S_456")).thenReturn("SEK");
        when(bankService.convert(new Money(10, "SEK"), "SEK")).thenReturn(new Money(10, "SEK"));
        when(bankService.hasSufficientFundsFor(new Money(10, "SEK"), "E_123")).thenReturn(false);
        verify(bankService, never()).withdraw(anyMoney(), anyAccount());
    }

    private Money anyMoney() {
        return (Money) any();
    }

    private String anyAccount() {
        return anyString();
    }
}