package stringcalc;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class StringCalculatorTest {

    // "" -> 0
    // "1" -> 1
    // "1, 2" -> 3
    // null -> IllegalArgumentException

    @Test
    public void emptyString_returnsZero() throws Exception {
        StringCalculator c = new StringCalculator();

        int result = c.add("");

        assertThat(result, is(0));
    }

    @Test
    public void singleNumber_returnsItsValue() throws Exception {
        StringCalculator c = new StringCalculator();

        int result = c.add("1");

        assertThat(result, is(1));
    }

    @Test
    public void multipleNumbers_returnsTheSum() throws Exception {
        StringCalculator c = new StringCalculator();

        int result = c.add("1, 2");

        assertThat(result, is(3));
    }

    @Test(expected = IllegalArgumentException.class)
    public void null_throws() throws Exception {
        StringCalculator c = new StringCalculator();

        c.add(null);
    }
}