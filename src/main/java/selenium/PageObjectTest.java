package selenium;

import org.junit.Test;
import selenium.models.User;
import selenium.pages.FormPage;
import selenium.pages.ListPage;
import selenium.pages.LoginPage;
import selenium.pages.MenuPage;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

public class PageObjectTest {

    private static final String USERNAME = "user";
    private static final String CORRECT_PASSWORD = "1";
    private static final String WRONG_PASSWORD = "2";

    @Test
    public void loginFailsWithFalseCredentials() {
        LoginPage loginPage = LoginPage.goTo();
        loginPage.logInWithExpectingFailure(USERNAME, WRONG_PASSWORD);
        assertThat(loginPage.getErrorMessage(), is(notNullValue()));
    }

    @Test
    public void loginSucceedsWithCorrectCredentials() {
        LoginPage loginPage = LoginPage.goTo();
        MenuPage menu = loginPage.logInWithExpectingSuccess(USERNAME, CORRECT_PASSWORD);
        assertThat(menu, is(notNullValue()));
    }

     @Test
     public void logoutSucceeds() {
         LoginPage loginPage = LoginPage.goTo();
         MenuPage menu = loginPage.logInWithExpectingSuccess(USERNAME, CORRECT_PASSWORD);
         assertThat(menu.isLogout(), is(true));
     }

     @Test
     public void userExistsWithGivenCredentials() {
         LoginPage loginPage = LoginPage.goTo();
         MenuPage menu = loginPage.logInWithExpectingSuccess(USERNAME, CORRECT_PASSWORD);
         ListPage listPage = menu.listPageButton();
         User user = new User("user", "1");
         assertThat(listPage.getUsers().contains(user), is(true));
     }

     @Test
     public void canAddNewUsers() {
         LoginPage loginPage = LoginPage.goTo();
         MenuPage menu = loginPage.logInWithExpectingSuccess(USERNAME, CORRECT_PASSWORD);
         FormPage page = menu.formPageButton();
         MenuPage menu2 = page.addUser("test", "pass");
         assertThat(menu2, is(notNullValue()));
     }


}
