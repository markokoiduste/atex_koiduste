package selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Marko on 12-Jan-17.
 */
public class FormPage extends AbstractPage {
    public FormPage(WebDriver driver) {
        super(driver);
        if (elementById("form_page") == null) {
            throw new IllegalStateException("not on form page");
        }
    }

    public MenuPage addUser(String user, String pass) {
        elementById("username_box").sendKeys(user);
        elementById("password_box").sendKeys(pass);
        elementById("add_button").click();
        WebElement element = elementById("menu_page");
        if (element != null) {
            return new MenuPage(this.driver);
        } else {
            throw new IllegalStateException("Adding new user failed!");
        }
    }
}
