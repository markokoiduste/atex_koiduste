package selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends AbstractPage {

    public static final String ID = "login_page";

    public LoginPage(WebDriver driver) {
        super(driver);
        if (elementById("login_page") == null) {
            throw new IllegalStateException("no login page");
        }
    }

    private void login(String user, String pass) {
        driver.get(BASE_URL);
        elementById("username_box").sendKeys(user);
        elementById("password_box").sendKeys(pass);
        elementById("log_in_button").click();
    }

    public LoginPage logInWithExpectingFailure(String user, String pass) {
        login(user, pass);

        if (getErrorMessage() != null) {
            return this;
        }

        throw new IllegalStateException("no error message");
    }

    public MenuPage logInWithExpectingSuccess(String user, String pass) {
        login(user, pass);
        if(getErrorMessage() != null) {
            throw new IllegalStateException("Login failed!");
        }
        return new MenuPage(driver);
    }

    public String getErrorMessage() {
        WebElement element = elementById("error_message");
        return element == null ? null : element.getText();
    }

    public static LoginPage goTo() {
        WebDriver driver = getDriver();
        driver.get(BASE_URL);
        return new LoginPage(driver);
    }

}
