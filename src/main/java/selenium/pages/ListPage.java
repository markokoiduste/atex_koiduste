package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import selenium.models.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marko on 12-Jan-17.
 */
public class ListPage extends AbstractPage {
    public ListPage(WebDriver driver) {
        super(driver);
        if (elementById("list_page") == null) {
            throw new IllegalStateException("not on list page");
        }
    }

    public List<User> getUsers() {
        List<User> users = new ArrayList<>();
        List<WebElement> rows = elementById("user_list")
                .findElements(By.tagName("div"));
        for (WebElement row : rows) {
            users.add(new User(row.getAttribute("username"), row.getAttribute("password")));
        }
        return users;
    }
}
