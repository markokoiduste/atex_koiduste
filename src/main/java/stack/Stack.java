package stack;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Created by Marko on 16-Sep-16.
 */
public class Stack {
    private Integer[] data;
    private int index;

    public Stack(int size) {
        this.data = new Integer[size];
        this.index = -1;
    }

    public void push(Integer i) {
        if (this.data.length > index) this.data[++index] = i;
        else throw new IllegalStateException("Index out of bounds");
    }

    public Integer pop() {
        if (index >= 0) return this.data[index--];
        else throw new IllegalArgumentException("Index out of bounds");
    }

    public Integer peek() {
        if (index >= 0 && index < data.length) return this.data[index];
        else throw new IllegalArgumentException("Index out of bounds");
    }

    public Integer size() {
        int count = 0;
        for (int i = index; i >= 0; i--) {
            if (data[i] != null) count++;
        }

        return count;
    }
}
