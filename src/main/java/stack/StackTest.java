package stack;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

@SuppressWarnings("unused")
public class StackTest {

    @Test
    public void newStackHasNoElements() {
        Stack stack = new Stack(100);
        assertThat(stack.size(), is(0));
    }

    @Test
    public void pushStackIncreasesStackSize() {
        Stack stack = new Stack(100);

        stack.push(0);
        stack.push(-100);
        assertThat(stack.size(), is(2));
    }

    @Test
    public void pushTwoPopTwoKeepsStackEmpty() {
        Stack stack = new Stack(100);

        stack.push(-100);
        stack.push(100);
        stack.pop();
        stack.pop();
        assertThat(stack.size(), is(0));
    }

    @Test
    public void popReturnsCorrectElements() {
        Stack stack = new Stack(100);

        stack.push(35);
        stack.push(-37);
        assertThat(stack.pop(), is(-37));
        assertThat(stack.pop(), is(35));
    }

    @Test
    public void peekShowsCorrectElement() {
        Stack stack = new Stack(100);

        stack.push(34);
        stack.push(-4);
        assertThat(stack.peek(), is(-4));
    }

    @Test
    public void peekShowsCorrectElementAndStackHasCorrectNumberOfElements() {
        Stack stack = new Stack(100);

        stack.push(143);
        stack.push(-124);
        assertThat(stack.peek(), is(-124));
        assertThat(stack.size(), is(2));
    }

    @Test
    public void peekingTwiceShowsTheSameElement(){
        Stack stack = new Stack(100);

        stack.push(100);
        stack.push(-5);
        assertThat(stack.peek(), is(-5));
        assertThat(stack.peek(), is(-5));
    }

    @Test(expected = IllegalArgumentException.class)
    public void popFromEmptyStackThrowsException() {
        Stack stack = new Stack(100);
        stack.pop();
    }

    @Test(expected = IllegalArgumentException.class)
    public void peekFromEmptyStackThrowsException() {
        Stack stack = new Stack(100);
        stack.peek();
    }

    @Test
    public void lifoTestOnStack() {
        Stack stack = new Stack(100);

        stack.push(1);
        stack.push(2);
        stack.push(3);
        assertThat(stack.pop(), is(3));
        assertThat(stack.pop(), is(2));
        assertThat(stack.pop(), is(1));
    }
}